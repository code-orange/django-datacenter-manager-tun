# Generated by Django 2.2 on 2019-04-16 11:44

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_datacenter_manager_tun", "0004_add_tunnel_application_and_mode"),
    ]

    operations = [
        migrations.AddField(
            model_name="dctunconnection",
            name="side",
            field=models.IntegerField(
                choices=[(0, "SERVER"), (1, "CLIENT"), (2, "EQUAL")], default=0
            ),
        ),
    ]
