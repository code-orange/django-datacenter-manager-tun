# Generated by Django 2.2 on 2019-04-16 10:14

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_datacenter_manager_tun", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="dctunconnection",
            name="ipv4",
        ),
        migrations.RemoveField(
            model_name="dctunconnection",
            name="ipv6",
        ),
        migrations.AddField(
            model_name="dctunconnection",
            name="cmdb_host",
            field=models.CharField(default="invalid", max_length=255),
            preserve_default=False,
        ),
    ]
