from django.apps import apps
from django.contrib import admin

for model in apps.get_app_config("django_datacenter_manager_tun").models.values():
    admin.site.register(model)
