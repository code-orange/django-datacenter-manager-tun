from django_mdat_customer.django_mdat_customer.models import *


class DcTunConnection(models.Model):
    SERVER = 0
    CLIENT = 1
    EQUAL = 2

    SIDE_CHOICES = (
        (SERVER, "SERVER"),
        (CLIENT, "CLIENT"),
        (EQUAL, "EQUAL"),
    )

    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, db_column="customer")
    cmdb_host = models.CharField(max_length=255)
    tunnel_type = models.ForeignKey(
        "DcTunTypes", models.DO_NOTHING, db_column="tunnel_type"
    )
    side = models.IntegerField(choices=SIDE_CHOICES, default=SERVER)
    related_to = models.ForeignKey(
        "self", models.DO_NOTHING, db_column="related_to", null=True, blank=True
    )

    class Meta:
        db_table = "dc_tun_connection"


class DcTunOptions(models.Model):
    tunnel = models.ForeignKey(DcTunConnection, models.DO_NOTHING, db_column="tunnel")
    option = models.CharField(max_length=200)
    value = models.TextField()

    class Meta:
        db_table = "dc_tun_options"


class DcTunTypes(models.Model):
    name = models.CharField(max_length=50)
    application = models.CharField(max_length=50)
    mode = models.CharField(max_length=50)

    class Meta:
        db_table = "dc_tun_types"
